from math import sqrt

a = float(input("Insira um valor para A: "))
b = float(input("Insira um valor para B: "))
c = float(input("Insira um valor para C: "))

d = b ** 2 - 4 * a * c
print("valor do delta: ", d)


if d > 0:
    delta = math.sqrt(d)
    print("Raiz quadrada de delta: ", delta)
    x1 = (- b + delta) / 2 * a
    x2 = (- b - delta) / 2 * a
    print("As raizes são: ", x1, "e", x2)

elif d == 0:
	delta = math.sqrt(d)
    print("Raiz quadrada de delta: ", delta)
    x = -b / (2 * a)
    print("X = ", x)
 
elif d < 0:
    print("A raiz nesse caso é negativa, menor que zero.")
