print("---- CALCULADORA ----")

sair = False

while sair == False:

  num1 = input("Digite o primeiro número: ")
  num1 = int(num1)
  operador = input("Digite o operador a usar: ")
  num2 = input("Digite o segundo número: ")
  num2 = int(num2)

  if operador == '+':
    operacao = (num1 + num2)
  if operador == '-':
    operacao = num1 - num2
  if operador == '*':
    operacao = num1 * num2
  if operador == '%':
    operacao = num1 / num2

  print ("O resultado é: ", operacao)

  teste = input("Deseja sair do programa? (sim ou nao)")
  if teste == "sim":
    sair = True
    print("Programa encerrado! :)")
