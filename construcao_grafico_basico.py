#biblioteca
import matplotlib.pyplot as plt

x = [1, 3, 5, 9, 2]
y = [7, 9, 11, 5, 6]

titulo = ("Gráfico de Pontos!")
eixox = ("Eixo X")
eixoy = ("Eixo Y")
#título
plt.title(titulo)
#eixos
plt.xlabel(eixox)
plt.ylabel(eixoy)

plt.plot(x, y, color = "k", linestyle = "-.")
plt.scatter(x, y, label = "meus pontos", color = "r", marker = ".", s = 500)
plt.legend()
plt.show()
